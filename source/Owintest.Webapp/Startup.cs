﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using System.Diagnostics;
using System.Web.Http;

[assembly: OwinStartup(typeof(Owintest.Webapp.Startup))]

namespace Owintest.Webapp
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.Use(async (context, next) =>
            {
                Debug.WriteLine(context.Request.Uri);
                await next();
                Debug.WriteLine(context.Response.StatusCode);
            });

            HttpConfiguration config = new HttpConfiguration();
            config.MapHttpAttributeRoutes();
            app.UseWebApi(config);
        }
    }
}

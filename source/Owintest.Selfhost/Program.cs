﻿using Microsoft.Owin.Hosting;
using Owintest.Webapp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Owintest.Selfhost
{
    class Program
    {
        static void Main(string[] args)
        {
            using (WebApp.Start<Startup>(url: "http://localhost:9000/"))
            {
                Console.WriteLine("Host running");
                Console.ReadLine();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Owintest.Webapp
{
    public class HomeController : ApiController
    {
        [Route("api")]
        public object Get()
        {
            return "Hello world!";
        }
    }
}